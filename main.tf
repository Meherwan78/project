module "web" {
  source           = "github.com/MEHERWAN04/terraform-provisioner.git"
  region           = var.region
  ami              = var.ami
  instance_type    = var.instance_type
  key_name         = var.key_name
  private_key_path = var.private_key_path

}